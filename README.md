[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
[![Pipeline Status](https://gitlab.com/tawasta/odoo/product/badges/14.0-dev/pipeline.svg)](https://gitlab.com/tawasta/odoo/product/-/pipelines/)

Product
=======
Product management

[//]: # (addons)

Available addons
----------------
addon | version | maintainers | summary
--- | --- | --- | ---
[coupon_product_set_company](coupon_product_set_company/) | 14.0.1.0.0 |  | Sets company field for coupon product
[product_analytic_tags](product_analytic_tags/) | 14.0.1.0.0 |  | Analytic tags for products and categories
[product_archive_product_archive_mrp_parameter](product_archive_product_archive_mrp_parameter/) | 14.0.1.0.0 |  | Archive Product and archive its MRP area parameter
[product_atex](product_atex/) | 14.0.1.0.0 |  | Add a field for product ATEX compliancy
[product_auditlog_rules](product_auditlog_rules/) | 14.0.1.1.0 |  | Audit log rules for product.product, product.template and product.category
[product_cant_order](product_cant_order/) | 14.0.1.0.2 |  | Product can't order
[product_competitor_alternative](product_competitor_alternative/) | 14.0.1.0.0 |  | New field for info about competitor's alternative product
[product_materials](product_materials/) | 14.0.1.0.2 |  | Product Materials model to be used on products
[product_name_disable_translate](product_name_disable_translate/) | 14.0.1.0.0 |  | Removes the option to translate product names
[product_on_hand_qty_color](product_on_hand_qty_color/) | 14.0.1.0.0 |  | Show color if stock belongs to several locations
[product_pricelist_disable_company_check](product_pricelist_disable_company_check/) | 14.0.1.0.0 |  | Don't force using same company for product and product pricelist
[product_publication](product_publication/) | 14.0.1.0.0 |  | Adds publication attributes for products.
[product_quality_instruction](product_quality_instruction/) | 14.0.1.0.0 |  | Instruction Documents for Products
[product_quality_instruction_incoming_shipments](product_quality_instruction_incoming_shipments/) | 14.0.1.0.0 |  | Mandatory quality checks when receiving goods
[product_reach](product_reach/) | 14.0.1.0.0 |  | Add a field for product REACH compliancy
[product_rohs](product_rohs/) | 14.0.1.0.0 |  | Add a field for product RoHS compliancy
[product_show_only_in_suggested_accessories](product_show_only_in_suggested_accessories/) | 14.0.1.0.1 |  | Product Show only in Suggested accessories
[product_storage_card](product_storage_card/) | 14.0.1.0.0 |  | Printable product storage card
[product_supplierinfo_for_customer_code_field](product_supplierinfo_for_customer_code_field/) | 14.0.1.0.0 |  | Show all customer codes in a single field
[product_template_attribute_value_ids_hide](product_template_attribute_value_ids_hide/) | 14.0.1.0.0 |  | Hides the product_template_attribute_value_ids by default on product tree
[product_template_dimension](product_template_dimension/) | 14.0.1.0.0 |  | Variant dimensions are managed in related product template
[product_template_hide_sh_tags](product_template_hide_sh_tags/) | 14.0.1.0.0 |  | Hide SH tags from product template
[product_template_rack](product_template_rack/) | 14.0.1.0.0 |  | Introduces a new field, Rack, to set stable product locations
[product_template_row](product_template_row/) | 14.0.1.0.0 |  | Introduces a new field, Row, to set stable product locations
[product_template_weight](product_template_weight/) | 14.0.1.0.0 |  | Variant weight and volume is managed in related product template
[product_translations](product_translations/) | 14.0.1.0.0 |  | Product translations in backend
[product_tree_description](product_tree_description/) | 14.0.1.0.0 |  | Description field in product tree view
[product_unspsc](product_unspsc/) | 14.0.1.0.0 |  | New field for UNSPSC Code
[product_update_quantity_to_inventory_adjustment](product_update_quantity_to_inventory_adjustment/) | 14.0.1.0.0 |  | Use Inventory Adjustment from Update Quantity functionality
[product_use_tree_as_default_view](product_use_tree_as_default_view/) | 14.0.1.0.0 |  | Use tree as a default view for products
[product_variant_company](product_variant_company/) | 14.0.1.0.0 |  | Add varian_company_id for product variant
[product_variant_sequence](product_variant_sequence/) | 14.0.1.0.0 |  | Order Product Variants based on sequence_variant.
[product_vendor_codes_field](product_vendor_codes_field/) | 14.0.1.0.1 |  | Show all vendor codes in a single field
[uom_auditlog_rules](uom_auditlog_rules/) | 14.0.1.0.0 |  | Adds audit log rules for uom.uom and uom.category

[//]: # (end addons)
